/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author bbnpo
 */
public class Product {

    private int id;
    private String productName;
    private float productPrice;
    private String productSize;
    private String productSweet;
    private String productType;
    private int cateId;

    public Product(int id, String productName, float productPrice, String productSize, String productSweet, String productType, int cateId) {
        this.id = id;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productSize = productSize;
        this.productSweet = productSweet;
        this.productType = productType;
        this.cateId = cateId;
    }

    public Product(String productName, float productPrice, String productSize, String productSweet, String productType, int cateId) {
        this.id = -1;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productSize = productSize;
        this.productSweet = productSweet;
        this.productType = productType;
        this.cateId = cateId;
    }

    public Product() {
        this.id = -1;
        this.productName = "";
        this.productPrice = 0;
        this.productSize = "";
        this.productSweet = "";
        this.productType = "";
        this.cateId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public float getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(float productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductSize() {
        return productSize;
    }

    public void setProductSize(String productSize) {
        this.productSize = productSize;
    }

    public String getProductSweet() {
        return productSweet;
    }

    public void setProductSweet(String productSweet) {
        this.productSweet = productSweet;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public int getCateId() {
        return cateId;
    }

    public void setCateId(int cateId) {
        this.cateId = cateId;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", productName=" + productName + ", productPrice=" + productPrice + ", productSize=" + productSize + ", productSweet=" + productSweet + ", productType=" + productType + ", cateId=" + cateId + '}';
    }

    public static Product fromRS(ResultSet rs) {
        Product product = new Product();
        try {
            product.setId(rs.getInt("product_id"));
            product.setProductName(rs.getString("product_name"));
            product.setProductPrice(rs.getFloat("product_price"));
            product.setProductSize(rs.getString("product_size"));
            product.setProductSweet(rs.getString("product_sweet_level"));
            product.setProductType(rs.getString("product_type"));
            product.setCateId(rs.getInt("category_id"));

        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return product;
    }

}
